DOCKER + APACHE + ORACLE + PHP7.4 (DAOP)
========================================

PREREQS (MAC):

Ensure latest version of Docker (4.7.1), in Docker Settings:

- In "General": Use Docker Compose V2 should be selected (warning: this will break dockstation)
- In "Experimental Features": Ensure both checks are selected

Make sure to hit "Apply & Restart"

TO USE:

`docker compose build`
`docker compose up -d`

Wait for Oracle DB to startup

Head to localhost:3000 (or localhost:3443)

TO DEV:

`docker compose exec web bash` to access php/apache server
`docker compose exec oracledb bash` to access oracledb server